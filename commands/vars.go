package commands

import (
	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
)

var (
	gaControlAdd    func()
	gaControlRemove func()
	rID             string
	rClient         *redis.Client
	msg             *discordgo.MessageEmbed
	gaActive        = false

	// Polls Vars
	poll
)
