package commands

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/ucgc/discord/globalScope"
)

func HelpMain(cxt *globalScope.Context) {
	emf := make([]*discordgo.MessageEmbedField, 0)

	emf = append(emf, &discordgo.MessageEmbedField{
		Name:  "Giveaways!",
		Value: "``newgiveaway <item>`` : Will create a new giveaway event, controlled by a reaction. \n``drawwinner`` : Will stop collecting entrants and randomly select a winner. \n\n Giveaways require the user to have at least the: ``Manage Server`` permission.",
	})

	emf = append(emf, &discordgo.MessageEmbedField{
		Name:  "Music!",
		Value: "``play (search text | youtube link)``: Plays audio in the current voice channel you are in. Primary service is Youtube, with others on the way. \n\n``skip`` : Skips the current playing song \n\n``stop``: Halts all playing music and leaves the channel, clearing the queue.",
	})

	emf = append(emf, &discordgo.MessageEmbedField{
		Name:  "Rolling",
		Value: "``roll``: will pick a random number in the range of 1-100.",
	})

	emf = append(emf, &discordgo.MessageEmbedField{
		Name:  "Cat",
		Value: "``cat``: will search random.cat for an image then provide it for all to enjoy.",
	})

	emf = append(emf, &discordgo.MessageEmbedField{
		Name:  "Ping",
		Value: "``ping``: returns \"pong\", this is used as a spot check to see if the bot is functioning.",
	})

	cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, &discordgo.MessageEmbed{
		Title:  "Help Screen",
		Color:  838993,
		Fields: emf,
	})
}
