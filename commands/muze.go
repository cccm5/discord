package commands

import (
	"errors"

	"github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/ucgc/discord/globalScope"
	"gitlab.com/ucgc/discord/services"
	"gitlab.com/ucgc/discord/services/music"
)

var (
	//voiceInstances allows usuage of multiple instances
	voiceInstances map[string]*music.VoiceInstance
	err            error
	firstRun       bool
)

// JoinMusic provides a proxy for other commands to join a channel but also directly
func joinMusic(cxt *globalScope.Context) error {
	if voiceInstances == nil {
		voiceInstances = make(map[string]*music.VoiceInstance)
	}
	joinedInstance, err := cxt.Session.ChannelVoiceJoin(cxt.Guild.ID, cxt.SearchVoice(), false, true)
	if err != nil {
		logrus.Error("Error Joining Voice Channel: ", err)
		return errors.New("Unable to Join")
	}

	singleInstance := new(music.VoiceInstance)
	singleInstance.Guild = cxt.Guild
	singleInstance.Session = cxt.Session
	singleInstance.Channel = cxt.Channel
	singleInstance.Voice = joinedInstance
	voiceInstances[cxt.Guild.ID] = singleInstance

	if music.SongChannel == nil {
		music.SongChannel = make(chan *music.Song)
	}

	singleInstance.InChannel = true

	logrus.Debug("New Voice Instace Spawned")
	return nil
}

// PlayMusic is the primary Driver for music
func PlayMusic(cxt *globalScope.Context) {
	if voiceInstances == nil || voiceInstances[cxt.Guild.ID] == nil {
		err := joinMusic(cxt)
		if err != nil {
			return
		}
	}

	if !voiceInstances[cxt.Guild.ID].Voice.Ready {
		logrus.Error("Voice Instance is no longer active")
		voiceInstances[cxt.Guild.ID].Voice = nil
		delete(voiceInstances, cxt.Guild.ID)
		joinMusic(cxt)
	}

	song, err := services.QueryYoutube(cxt)
	if err != nil || song == nil {
		logrus.Error("Cannot use Youtube: ", err)
		return
	}

	if music.SongChannel == nil {
		music.SongChannel = make(chan *music.Song)
	}

	go func() {
		voiceInstances[cxt.Guild.ID].PlayQueue()
	}()

	music.SongChannel <- song
}

// StopMusic will stop all running music
func StopMusic(cxt *globalScope.Context) {
	voiceInstances[cxt.Guild.ID].Stop()
	voiceInstances[cxt.Guild.ID].Voice = nil
	delete(voiceInstances, cxt.Guild.ID)

	cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, &discordgo.MessageEmbed{
		Title:       "Music Service!",
		Description: "Stopping music and clearing queue as requested by: <@" + cxt.User.ID + ">",
		Color:       0x495BFF,
	})
}

// SkipMusic will skip the current playing song
func SkipMusic(cxt *globalScope.Context) {
	voiceInstances[cxt.Guild.ID].Skip()
	cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, &discordgo.MessageEmbed{
		Title:       "Music Service!",
		Description: "Skipping Song as requested by: <@" + cxt.User.ID + ">",
		Color:       0x495BFF,
	})
}
