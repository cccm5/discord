package commands

import (
	"math/rand"

	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ucgc/discord/globalScope"
)

func RollDice(cxt *globalScope.Context) {
	rolledNum := rand.Intn(100)

	cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, &discordgo.MessageEmbed{
		Title:       "Random Roll!",
		Description: "<@" + cxt.User.ID + "> rolled a " + strconv.Itoa(rolledNum) + "! :game_die:",
	})
}
