package commands

import (
	"fmt"

	"math/rand"
	"strings"

	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ucgc/discord/globalScope"
)

func StartRaffle(cxt *globalScope.Context) {
	if !cxt.CheckPermission(discordgo.PermissionManageServer) {
		cxt.PermissionMsgSend("Manage Server")
		return
	}

	if gaActive {
		cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, &discordgo.MessageEmbed{
			Title:       "Error Occured",
			Description: "There is a currently active raffle please end the current one.",
			Color:       0xff0f0f,
		})
		return
	}

	if len(cxt.Command.Args) < 1 {
		cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, &discordgo.MessageEmbed{
			Title:       "Error Occured",
			Description: "You did not provide a prize name. Please add one.",
			Color:       0xff0f0f,
		})
		return
	}

	rClient = cxt.Redis

	msg = &discordgo.MessageEmbed{
		Title:       "A new Giveaway has begun!",
		Description: "A new gaffle for " + cxt.Command.Query + ": Enter by clicking on the :gift: reaction!",
		Color:       0x1971ff,
	}

	msg.Fields = make([]*discordgo.MessageEmbedField, 0)

	msg.Fields = append(msg.Fields, &discordgo.MessageEmbedField{
		Name:   "Entrants",
		Value:  "UCGC",
		Inline: true,
	})

	sentMsg, _ := cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, msg)
	err := cxt.Session.MessageReactionAdd(cxt.Channel.ID, sentMsg.ID, "🎁")
	if err != nil {
		fmt.Println(err)
	}

	rID = sentMsg.ID

	gaControlAdd = cxt.Session.AddHandler(watchRaffleReactionsAdd)
	gaControlRemove = cxt.Session.AddHandler(watchRaffleReactionsRemove)
	gaActive = true
}

func StopRaffle(cxt *globalScope.Context) {
	if !cxt.CheckPermission(discordgo.PermissionManageMessages) {
		cxt.PermissionMsgSend("Manage Messages")
		return
	}

	gaControlAdd()
	gaControlRemove()
	msgData, _ := cxt.Session.ChannelMessageSend(cxt.Channel.ID, "Raffle Stopped")

	gaActive = false

	go func() {
		time.Sleep(1 * time.Second)
		entrants, _ := rClient.SMembers("raffleMembers").Result()

		cxt.Session.ChannelMessageEdit(cxt.Channel.ID, msgData.ID, "Picking Winner.")
		time.Sleep(time.Second / 4)
		cxt.Session.ChannelMessageEdit(cxt.Channel.ID, msgData.ID, "Picking Winner..")
		time.Sleep(time.Second / 4)
		cxt.Session.ChannelMessageEdit(cxt.Channel.ID, msgData.ID, "Picking Winner...")
		time.Sleep(time.Second / 4)
		cxt.Session.ChannelMessageEdit(cxt.Channel.ID, msgData.ID, "Picking Winner....")
		time.Sleep(time.Second / 4)

		if len(entrants) == 0 {
			cxt.Session.ChannelMessageEdit(cxt.Channel.ID, msgData.ID, "No Winner! (There are no entrants)")
			return
		}
		winner := entrants[rand.Intn(len(entrants))]

		cxt.Session.ChannelMessageEdit(cxt.Channel.ID, msgData.ID, "Winner! "+winner)
		rClient.Del("raffleMembers").Result()

	}()
}

func watchRaffleReactionsAdd(session *discordgo.Session, reactionAdd *discordgo.MessageReactionAdd) {
	botID, _ := session.User("@me")

	if reactionAdd.MessageID == rID && reactionAdd.UserID != botID.ID {
		rClient.SAdd("raffleMembers", "<@"+reactionAdd.UserID+">")
		entrants, _ := rClient.SMembers("raffleMembers").Result()

		msg.Fields[0].Value = strings.Join(entrants, "\n")
		session.ChannelMessageEditEmbed(reactionAdd.ChannelID, rID, msg)
	}
}

func watchRaffleReactionsRemove(session *discordgo.Session, reactionRemove *discordgo.MessageReactionRemove) {
	botID, _ := session.User("@me")

	if reactionRemove.MessageID == rID && reactionRemove.UserID != botID.ID {
		rClient.SRem("raffleMembers", "<@"+reactionRemove.UserID+">")
		entrants, _ := rClient.SMembers("raffleMembers").Result()

		if len(entrants) == 0 {
			msg.Fields[0].Value = "No Entries!"
		} else {
			msg.Fields[0].Value = strings.Join(entrants, "\n")
		}

		session.ChannelMessageEditEmbed(reactionRemove.ChannelID, rID, msg)
	}
}

type RaffleInfo struct {
	UserName   string
	UserID     string
	Prize      string
	Entrants   []string
	IsFinished bool
	MsgID      string
}
