package commands

import "gitlab.com/ucgc/discord/globalScope"
import "github.com/bwmarrin/discordgo"
import "time"

func Ping(context *globalScope.Context) {
	permissionNeeded := discordgo.PermissionManageMessages

	if !context.CheckPermission(permissionNeeded) {
		context.PermissionMsgSend("Manage Messages")
		return
	}

	context.Session.ChannelMessageSend(context.Channel.ID, "Pong!")
}

func Rules(cxt *globalScope.Context) {
	if !cxt.CheckPermission(0) {
		cxt.PermissionMsgSend("Administrator")
		return
	}
	emf := make([]*discordgo.MessageEmbedField, 0)

	rules := `1. Treat others respectfully. Personal attacks including acts of discrimination and any other forms of cyberbullying directed at other members of the club will not be tolerated.

2. Keep content appropriate - if you don't want your parents seeing it, don't post it.

3. Do not spam. BUT if you must, take it to <#247932188388098048> 

4. Lastly, don’t be afraid to step outside of your comfort zone! Be yourself and have fun!

**Questions?** If you have any questions or concerns, feel free to communicate them with any one of our tagged officers.
	`

	starting := `1) Make sure you set your display picture so you don't look like a 1900's game art and correct your settings by clicking the gear in the bottom left.
	
2) Please fill out this forms so we know if you are interested in our club! We need to know what you are interested so we can tailor our events to your needs :wink:

http://bit.ly/UCGC2017
http://bit.ly/TespaUCGC2017

3) Introduce yourself in <#213421967598944256> to let others know that you are here for business! Tell them your favorite games, request a game tag, tell a little about yourself, and what you do on your spare time (better be making dank memes)!

4) You can mute and collapse any channels that are annoying you and draining your battery!
`

	contact := `UConn Gaming Club Information:
Email: UConnGC@gmail.com
UConn Gaming Club Twitter: https://twitter.com/UConnGamingClub
UConn Gaming Club Facebook: https://www.facebook.com/groups/uconngamingclub/
	`
	emf = append(emf, &discordgo.MessageEmbedField{
		Name:   "**RULES**",
		Value:  rules,
		Inline: true,
	})

	emf = append(emf, &discordgo.MessageEmbedField{
		Name:   "**GETTING STARTED**",
		Value:  starting,
		Inline: true,
	})

	emf = append(emf, &discordgo.MessageEmbedField{
		Name:   "**CONTACT INFORMATION**",
		Value:  contact,
		Inline: true,
	})

	cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, &discordgo.MessageEmbed{
		Title:       "Permanent Invite Link: https://discord.gg/ucgc",
		Fields:      emf,
		Description: "We’ll be primarily using Discord for announcements, events and in-game chats! ",
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Updated as of: " + time.Now().Format(time.UnixDate),
		},
		Author: &discordgo.MessageEmbedAuthor{
			Name: "Welcome to the UConn Gaming Club Discord Server!",
		},
	})
}
