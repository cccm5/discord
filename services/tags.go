package services

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.com/ucgc/discord/dcontext"
)

// TagLiveStatus adds or removes a designated Live role
// context : The general payload passed through all commands
func TagLiveStatus(context *dcontext.Context) {
	roles := context.Guild.Roles

	liveRole := ""

	for _, role := range roles {
		if role.Name == "Live" {
			liveRole = role.ID
		}
	}

	presence, err := context.State.Presence(context.Guild.ID, context.User.ID)
	if err != nil {
		log.Error("Error in obtaining presence: ", err)
		return
	}

	if presence.Game != nil && presence.Game.Type == 1 {
		context.Session.GuildMemberRoleAdd(context.Guild.ID, context.User.ID, liveRole)
	} else {
		context.Session.GuildMemberRoleRemove(context.Guild.ID, context.User.ID, liveRole)
	}
}
