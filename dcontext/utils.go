package dcontext

import (
	"net"
	"strconv"
	"strings"

	"github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
	"gopkg.in/mgo.v2"
)

func init() {
	PermissionMap = map[string]int{
		"manageserver":   discordgo.PermissionManageServer,
		"administrator":  discordgo.PermissionAdministrator,
		"managemessages": discordgo.PermissionManageMessages,
		"manageroles":    discordgo.PermissionManageRoles,
	}
}

// BuildMessageContext creates the context based on a Message Event
func BuildMessageContext(s *discordgo.Session, message *discordgo.MessageCreate) (*Context, error) {
	channel, err := s.State.Channel(message.ChannelID)

	guild, err := s.State.Guild(channel.GuildID)

	return &Context{
		Session: s,
		State:   s.State,
		Guild:   guild,
		Channel: channel,
		User:    message.Author,

		Redis: RedisClient,
		Mongo: MongoClient,

		Command: ConvertToCommand(message.Content),
	}, err
}

//EstablishDatabase makes the connections to the database
func EstablishDatabase() error {
	rdaddr, _ := net.LookupIP(viper.GetString("redis.address"))

	var ra string
	for _, v := range rdaddr {
		ra += v.String()
	}

	RedisClient = redis.NewClient(&redis.Options{
		Addr:     ra + ":" + viper.GetString("redis.port"),
		Password: viper.GetString("redis.password"),
		DB:       viper.GetInt("redis.database"),
	})

	_, err := RedisClient.Ping().Result()

	mongoaddr, _ := net.LookupIP(viper.GetString("mongo.address"))

	var ma string
	for _, v := range mongoaddr {
		ma += v.String()
	}

	if MongoClient, err = mgo.Dial(ma + ":" + viper.GetString("mongo.port")); err == nil {
		err = MongoClient.DB("discord").Login(viper.GetString("mongo.user"), viper.GetString("mongo.password"))
	}

	return err
}

//SetUserData collects the value of the current user
func SetUserData(s *discordgo.Session) error {
	var err error
	BotUser, err = s.User("@me")
	return err
}

//MentionCheck returns true or false if the bot is mentioned
func MentionCheck(message string) bool {
	mentionNicknamed := "<@!" + BotUser.ID + ">"
	mentionRegular := "<@" + BotUser.ID + ">"

	if strings.HasPrefix(message, mentionRegular) || strings.HasPrefix(message, mentionNicknamed) {
		return true
	}

	return false
}

//ConvertToCommand will take the message content and converts to Command
func ConvertToCommand(message string) *Command {
	splitMessage := strings.Fields(message)[1:]

	if len(splitMessage) < 1 {
		return &Command{
			IsEmpty: true,
		}
	}

	if len(splitMessage) == 1 {
		return &Command{
			Root:    splitMessage[0],
			Args:    nil,
			Query:   "",
			IsEmpty: false,
		}
	}

	return &Command{
		Root:    strings.ToLower(splitMessage[0]),
		Args:    splitMessage[1:],
		Query:   strings.Join(splitMessage[1:], " "),
		IsEmpty: false,
	}
}

func PermissionCheck(cxt *Context, command *CommandDetails) bool {
	permission, _ := cxt.State.UserChannelPermissions(cxt.User.ID, cxt.Channel.ID)

	if permission&command.Permission == command.Permission {
		return true
	}
	return false
}

func AddBaseCommands() {
	value, _ := RedisClient.Get("baseAdded").Result()
	bvalue, err := strconv.ParseBool(value)
	if err != nil {
		logrus.Errorf("Unable to get key: %v", err)
		return
	}

	if bvalue {
		logrus.Info("Base commands exist, skipping...")
		return
	}

	addCmd := "db.add Adds new commands to the database"
	prxyCommand := &Command{
		Args: strings.Fields(addCmd),
	}

	prxyCxt := &Context{
		Command: prxyCommand,
		Mongo:   MongoClient,
		Redis:   RedisClient,
	}

	//Add the add command
	AddCommand(prxyCxt)

	// Add the delete Command
	deleteCmd := "db.delete Deletes a command from the database"
	prxyCommand.Args = strings.Fields(deleteCmd)
	AddCommand(prxyCxt)
	prxyCommand.Args = []string{"add", strconv.Itoa(discordgo.PermissionAdministrator)}
	UpdatePermissions(prxyCxt)

	updateAliasCmd := "db.update.alias Updates a command's aliases"
	prxyCommand.Args = strings.Fields(updateAliasCmd)
	AddCommand(prxyCxt)

	RedisClient.Set("baseAdded", "true", -1)
	logrus.Info("Base commands added...")
}
