package dcontext

import (
	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
	mgo "gopkg.in/mgo.v2"
)

// Context provides globally accessable infomation for the rest of the bot
type Context struct {
	Session *discordgo.Session
	State   *discordgo.State
	Guild   *discordgo.Guild
	Channel *discordgo.Channel
	User    *discordgo.User

	// Database Clients
	Redis *redis.Client
	Mongo *mgo.Session

	Command *Command
}

//Command holds the info needed for most command functions
type Command struct {
	Root    string
	Args    []string
	Query   string
	IsEmpty bool
}

//CommandDetails is the medium between Mongo and the Bot
type CommandDetails struct {
	Key               string
	Description       string
	RateLimited       bool
	Alias             []string
	Permission        int
	PermittedChannels []string
}

//
type CommandDB struct{}
