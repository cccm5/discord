package main

import (
	"os"
	"os/signal"

	"gitlab.com/ucgc/discord/dcontext"

	"github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetDefault("redis.database", 1)
}

func main() {
	if err := viper.ReadInConfig(); err != nil {
		logrus.Fatalf("Could not read config: %v", err)
	}

	if err := dcontext.EstablishDatabase(); err != nil {
		logrus.Fatalf("Could not connect to databases: %v", err)
	}

	session, err := discordgo.New("Bot " + viper.GetString("discordToken"))
	if err != nil {
		logrus.Fatalf("Failed to create Discord Session: %v", err)
	}

	session.AddHandler(messageEvent)
	session.AddHandler(presenceEvent)

	if err := session.Open(); err != nil {
		logrus.Fatalf("Failed to Open Websocket Connection: %v", err)
	}

	if err := dcontext.SetUserData(session); err != nil {
		logrus.Fatalf("Cannot find @me User Data: %v", err)
	}

	dcontext.AddBaseCommands()

	// Handle Graceful Shutdown
	logrus.Info("Discord Bot now running. Exit with CTRL+C")
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, os.Kill)

	if signal := <-sigChan; signal != nil {
		logrus.Infof("Recieved signal: %v", signal)
		logrus.Info("Shutting down Discord Bot")
	}
}
