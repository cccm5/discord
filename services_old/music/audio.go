package music

import (
	"io"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/oleiade/lane"

	"github.com/Sirupsen/logrus"

	"github.com/jonas747/dca"
)

var (
	done chan error
)

// PlayQueue will check for any songs in the SongChannel and play it
func (v *VoiceInstance) PlayQueue() {
	if v.Queue == nil {
		v.Queue = lane.NewQueue()
	}

	if song := <-SongChannel; song != nil {
		v.Queue.Enqueue(song)
		logrus.Debug("Enqueuing song: ", song.Title, " by: ", song.User.Username)
	}

	//This grabs songs from the Queue and plays them through FFMPEG
	go func() {
		if v.AudioMutex == nil {
			v.AudioMutex = new(sync.Mutex)
		}
		v.AudioMutex.Lock()
		defer v.AudioMutex.Unlock()

		for {
			if v.Queue.Size() == 0 {
				time.Sleep(500 * time.Millisecond)
				break
			}

			v.NowPlaying = *v.Queue.Dequeue().(*Song)
			v.Session.ChannelMessageSendEmbed(v.Channel.ID, &discordgo.MessageEmbed{
				Title:       "Music!",
				Description: "Next up! **" + v.NowPlaying.Title + "** is now playing thanks to <@" + v.NowPlaying.User.ID + ">",
				Color:       0xf4b342,
			})

			v.IsSpeaking = true
			v.Voice.Speaking(v.IsSpeaking)
			v.IsPaused = false

			v.useDCA(v.NowPlaying.VideoURL)
			v.IsSpeaking = false
			v.Voice.Speaking(false)
		}
	}()
}

// This is the FFMPEG driver
func (v *VoiceInstance) useDCA(url string) {
	options := dca.StdEncodeOptions
	options.RawOutput = true
	options.Bitrate = 96
	options.Application = "lowdelay"

	encodeSession, err := dca.EncodeFile(url, options)
	if err != nil {
		logrus.Error("Failed creating an encoding Session: ", err)
		return
	}

	v.Encoder = encodeSession
	done = make(chan error)
	stream := dca.NewStream(encodeSession, v.Voice, done)
	v.Stream = stream

	for {
		select {
		case err := <-done:
			if err != nil && err != io.EOF {
				logrus.Error("An error occured in stream: ", err)
				return
			}

			encodeSession.Cleanup()
			return
		}
	}
}

// Stop will cancel the currently playing song
func (v *VoiceInstance) Stop() {
	v.Voice.Disconnect()
	if v.Encoder != nil {
		done <- nil
	}
	v.Clear()
}

// Skip will skip a song if it is playing
func (v *VoiceInstance) Skip() {
	if v.IsSpeaking {
		if v.Encoder != nil {
			v.Encoder.Cleanup()
		}
	}
	return
}

// Pause will pause a song if it is playing
func (v *VoiceInstance) Pause() {
	v.IsPaused = true
	if v.Stream != nil {
		v.Stream.SetPaused(v.IsPaused)
	}
}

// Resume will resume a song that is paused
func (v *VoiceInstance) Resume() {
	v.IsPaused = false
	if v.Stream != nil {
		v.Stream.SetPaused(v.IsPaused)
	}
}

// Clear clears the queue
func (v *VoiceInstance) Clear() {
	for v.Queue.Size() != 0 {
		v.Queue.Dequeue()
	}
	logrus.Info("Queue cleared!")
}
